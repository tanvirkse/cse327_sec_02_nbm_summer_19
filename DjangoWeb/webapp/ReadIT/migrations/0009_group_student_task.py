# Generated by Django 2.2.3 on 2019-08-17 19:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ReadIT', '0008_auto_20190817_1853'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('Gr_name', models.CharField(max_length=50)),
                ('Gr_id', models.IntegerField(primary_key=True, serialize=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('task_id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('pdf', models.FileField(upload_to='ReadIT/pdf_files')),
                ('group_id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='ReadIT.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('st_id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('gr_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ReadIT.Group')),
                ('tsk_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ReadIT.Task')),
            ],
        ),
    ]
