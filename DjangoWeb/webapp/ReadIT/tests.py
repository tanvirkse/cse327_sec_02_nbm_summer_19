from django.test import TestCase
from django.urls import reverse
from .models import Group,Task,Student,Profile
# Create your tests here.
class GroupTestCase(TestCase):
    def setUp(self):
        Group.objects.create(Gr_user = 123 ,Gr_name = '123', Gr_id = 123)
        Group.objects.create(Gr_user = '0',Gr_name = '0',Gr_id ='0')

    def GroupTest(self):
        group = Group.object.get(Gr_id = 123)
        expected_group = f'{Group.Gr_id}'
        self.assertEquals(expected_group, 123)

    def test_post_list_view(self):
        response = self.client.get(reverse('Groups'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '123')
        self.assertTemplateUsed(response, 'home.html')    

class TaskTestCase(TestCase):
    def setUp(self):
        Task.objects.create(group_id = 123, task_id = 123 , name ='123', pdf='123456')

    def TaskTest(self):
        task = Task.object.get(group_id = 123)
        expected_task = f'{Task.group_id}'
        self.assertEquals(expected_task, 123)

    def test_post_list_view(self):
        response = self.client.get(reverse('Tasks'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '123')
        self.assertTemplateUsed(response, 'group.html')    

class StudentTestCase(TestCase):
    def setUp(self):
        Student.objects.create(gr_id = 55, tsk_id = 66, st_id =123, name ='123456', tsk_done=True)

    def StudentTest(self):
        task = Task.object.get(st_id = 123)
        expected_student = f'{Student.group_id}'
        self.assertEquals(expected_student, 123)

    def test_post_list_view(self):
        response = self.client.get(reverse('Students'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '123')
        self.assertTemplateUsed(response, 'group.html') 

class ProfileTestCase(TestCase):
    def setUp(self):
        Profile.objects.create(user=1, image='ReadIT/profile_pics')

    def ProfileTest(self):
        task = Task.object.get(user = 1)
        expected_profile = f'{Profile.user}'
        self.assertEquals(expected_profile, 1)

    def test_post_list_view(self):
        response = self.client.get(reverse('Profile'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '1')
        self.assertTemplateUsed(response, 'profile.html') 