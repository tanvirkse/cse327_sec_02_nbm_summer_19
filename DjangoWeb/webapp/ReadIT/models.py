from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='ReadIT/default.png', upload_to='ReadIT/profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)

class Group(models.Model):
    Gr_user = models.ForeignKey(User, on_delete=models.CASCADE)
    Gr_name = models.CharField(max_length=50)
    Gr_id = models.IntegerField(primary_key=True)
    
    def __str__(self):
        return f'{self.Gr_name,self.Gr_id}'     

    def get_absolute_url(self):
        return reverse('group-detail',kwargs={'pk': self.pk})  

class Task(models.Model):
    group_id = models.OneToOneField(Group, on_delete= models.CASCADE)
    task_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    pdf = models.FileField(upload_to = 'ReadIT/pdf_files')

    def __str__(self):
        return f'Task {self.task_id}'  

class Student(models.Model):
    gr_id = models.ForeignKey(Group, on_delete= models.CASCADE)
    tsk_id = models.ForeignKey(Task, on_delete= models.CASCADE)
    st_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    tsk_done = models.BooleanField(default=False)

    def __str__(self):
        return f'Student {self.st_id}'  
