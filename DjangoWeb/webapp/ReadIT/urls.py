from django.contrib import admin
from django.urls import path , include
from rest_framework import routers
from . import views
from . views import (

    GroupListView,
    GroupDetailView,
    GroupCreateView,
    GroupUpdateView,
    GroupDeleteView,
    UserGroupListView,
    TaskCreateView
)


router = routers.DefaultRouter()
router.register('_restframework', views.ProfileViewSet)

urlpatterns = [

    path("index",views.index,name="index"),
    path("signup",views.signup,name="signup"),
    path("contact",views.contact,name="contact"),
    path("profile",views.profile,name="profile"),
    path("group",views.group,name="group"),
    path("about",views.about,name="about"),
    path("task",views.task,name="task"),
    path("student",views.student,name="student"),
    path("",GroupListView.as_view(),name="home"),
    path('user/<str:username>', UserGroupListView.as_view(), name='user-groups'),
    path("group/<int:pk>/",GroupDetailView.as_view(),name="group-detail"),
    path("group/new/",GroupCreateView.as_view(),name='group-create'),
    path("task/new/",TaskCreateView.as_view(),name='task-create'),
    path("group/<int:pk>/update/",GroupUpdateView.as_view(),name="group-update"),
    path("group/<int:pk>/delete/",GroupDeleteView.as_view(),name="group-delete"),
    path('rest',include(router.urls)),
    path("api-auth/", include('rest_framework.urls', namespace='rest_framework')),


]