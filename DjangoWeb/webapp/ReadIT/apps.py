from django.apps import AppConfig


class ReaditConfig(AppConfig):
    name = 'ReadIT'

    def ready(self):
        import ReadIT.signals
