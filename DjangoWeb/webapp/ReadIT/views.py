from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from rest_framework import viewsets
from rest_framework import generics
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from .serializers import (
    ProfileSerializers,
    StudentSerializers,
    TaskSerializers,
    GroupSerializers
)
from .models import Profile, Group, Student, Task

#ProfileRestframework
class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all().order_by('id')
    serializer_class = ProfileSerializers

#ProfileRestframework
class GroupViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all().order_by('id')
    serializer_class = GroupSerializers

#ProfileRestframework
class TaskViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all().order_by('id')
    serializer_class = TaskSerializers   

#ProfileRestframework
class StudentViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all().order_by('id')
    serializer_class = StudentSerializers

#base
def index(request):
    return render(request, "index.html")     

#groups
def group(request):
    context = {
        'Groups': Group.objects.filter(Gr_user = request.user),
        'Tasks': Task.objects.all(),
        'Students':Student.objects.all(),
    }
    return render(request, "group.html", context)

#signup
def signup(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'signup.html', {'form': form})

#contact
def contact(request):
    return render(request, "contact.html")

#about
def about(request):
    return render(request, "about.html")

#editprofile
@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'profile.html', context)

#home
def home(request):
    context = {
        'Groups': Group.objects.filter(Gr_user = request.user)
    }
    return render(request, "home.html", context)

#GroupList
class GroupListView(ListView):
    model = Group
    template_name = 'home.html'
    context_object_name = 'Groups'  

#UserBasedView
class UserGroupListView(ListView):
    model = Group
    template_name = 'home.html'
    context_object_name = 'Groups'  

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Group.objects.filter(Gr_user=user)

#TaskList
def task(request):
    context = {
        'Tasks': Task.objects.all(),
        'Students':Student.objects.all(),
    }
    return render(request, "task.html", context)

#StudentList
def student(request):
    context = {
        'Tasks': Task.objects.all(),
        'Students':Student.objects.all(),
    }
    return render(request, "student.html", context)

#Groups
class GroupDetailView(DetailView):
    model = Group

#CreateGroups
class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    fields = ['Gr_name', 'Gr_id']

    def form_valid(self, form):
        form.instance.Gr_user = self.request.user
        return super().form_valid(form)

#UpdateGroups
class GroupUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Group
    fields = ['Gr_name', 'Gr_id']

    def form_valid(self, form):
        form.instance.Gr_user = self.request.user
        return super().form_valid(form) 

    def test_func(self):
        group = self.get_object()
        if self.request.user == group.Gr_user:
            return True
        return False

#DeleteGroups
class GroupDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Group
    success_url = '/'

    def test_func(self):
        group = self.get_object()
        if self.request.user == group.Gr_user:
            return True
        return False    

#CreateTask
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ['group_id', 'task_id', 'name', 'pdf']

    def form_valid(self, form):
        form.instance.group_id = self.request.Group.Gr_id
        return super().form_valid(form)
