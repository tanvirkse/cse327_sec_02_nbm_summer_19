from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import Profile,Group,Task,Student

admin.site.register(Profile)
admin.site.register(Group)
admin.site.register(Task)
admin.site.register(Student)