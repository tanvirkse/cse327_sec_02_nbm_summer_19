from rest_framework import serializers
from .models import (
    Profile,
    Group,
    Student,
    Task
)

class ProfileSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ('url','image')

class GroupSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('Gr_user', 'Gr_name', 'id')

class StudentSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student
        fields = ('gr_id', 'tsk_id', 'st_id', 'name')

class TaskSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ('group_id', 'task_id', 'name', 'pdf')